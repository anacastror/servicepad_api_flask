import os
from dotenv import load_dotenv

load_dotenv()

class Config:
    SERVER_NAME = "localhost:7001"
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True

    # Configuracion de base de datos
    SECRET_KEY = os.environ.get("SECRET_KEY", "") 
    SQLALCHEMY_DATABASE_URI = "postgresql://flask:flask@localhost/app_serv"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    TEMPLATE_FOLDER = "views/templates/"
    STATIC_FOLDER = "views/static/"


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    SERVER_NAME = "localhost:3001"
    SECRET_KEY = 'dev'
    DEVELOPMENT = True
    DEBUG = True
    TESTING = True
